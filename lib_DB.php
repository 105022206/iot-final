<?php
	class Data{
		private $location;
		private $time;
		function __construct($loca_tmp,$time_tmp){
			$this->location = $loca_tmp;
			$this->time = $time_tmp;
		}

		function GetLoc(){
			return $this->location;
		}

		function GetTime(){
			return $this->time;
		}
	}

	class DB{
		public $DataBase;
		function __construct(){

		}

		function RequestData(){
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,"https://campus.kits.tw/api/get/data/aa1eef14");
			curl_setopt($ch, CURLOPT_POST, 1);

			$postdata =  "Content-Length: 0";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);


			$arr_header[] = "Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE1ZDgwMzA3MDRiOGM2ZmYyNDEzY2Y5YzIzYzJjOTZhODA4NDY5NjI3N2VjOTlmZjY3NDVjNjA1NjFkNThkNGUxNDY0ZTIzMWYyOTE2YjNlIn0.eyJhdWQiOiIyIiwianRpIjoiYTVkODAzMDcwNGI4YzZmZjI0MTNjZjljMjNjMmM5NmE4MDg0Njk2Mjc3ZWM5OWZmNjc0NWM2MDU2MWQ1OGQ0ZTE0NjRlMjMxZjI5MTZiM2UiLCJpYXQiOjE1NjEwMzI4NzgsIm5iZiI6MTU2MTAzMjg3OCwiZXhwIjoxNTkyNjU1Mjc4LCJzdWIiOiIyNyIsInNjb3BlcyI6W119.hb-0MVzF6_6F_MHRxRfLZjARnf7bU_vI9Tp4tu42xe05BLiKWzH6cOywCfKzyzHx6guDbj0NSxsvnRSfbRxsJgINxJDFQ_oGGq9ZB0Ez_INdmytqlsU2y-9W63ThwAWUcT1v0uSwQInNJPNKdof7dcaI5NW3SM0IrJA-by9Vo8YKyO2pFWMHOWJt5WH_KVuL7U9d9d6_cufbyw0pEhw4wV2x8Bl26Y5IQLmKWGC7Qnp9rjNC-7j01i1PcJXh7hO-KSEvDMgU-HihKJYzwg5zIPSqUwkdfhji72UpQuKSD6QTHSsNDMP058FQ6SL9yfbT7JQJ0HxmcRrIBBifpZ56oMq4UlWxln1nVGFt8frzWY8CKYuaLC9d1GchLp7NR3of7z4FwPkx1vsrP31h6cgtzHdYUV2raQtalx4ZyvWe3jMPrp1_02ZEZUhFvl5S2xDUv2x5DoMjmRV9jHAixh_V0GTDzYY4weImhrPs3p23Tn3L01ehxX6bMglhn9DptZeCybRWNor_1xtJi0G2jhI6s5SPyuNqAV5o6MYg0VoCTtDGlI_rKBLaEjMatIKAOWit9jrND6slwGZDkXN8k5HHjfn0WOuISG_huF_cr4zSPN1KqhpNPDOTTHP6zRyEw_bfqu5JAkKmYjKOHISRuYDYMs5isS_tfwQilwgYaQJ5l1k";
			$arr_header[] = "Accept:application/json";

			curl_setopt($ch, CURLOPT_HTTPHEADER, $arr_header);


			// In real life you should use something like:
			// curl_setopt($ch, CURLOPT_POSTFIELDS, 
			//          http_build_query(array('postvar1' => 'value1')));

			// Receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close ($ch);


			$this->DivideRowData($server_output);
			
			// echo "<br>";
			// print_r(($this->DataBase[0])->GetTime());
			// echo "<br>";
			return 0;
		}

		function Get24Hr_Lat(){
			$Year = intval(date("Y"));
			$Month = intval(date("m"));
			$Day = intval(date("d"));
			$Hour = intval(date("H"));

			$Diff = $Hour - 24;
			$Day = $Day -1;
			$Hour = $Hour + $Diff;


			foreach ($this->DataBase as $element) {
				$time = $element->GetTime();

				$tmp_Y = intval(substr($time, 0,4));
				if($tmp_Y < $Year) continue;

				$tmp_M = intval(substr($time, 5,2));
				if($tmp_M < $Month) continue;

				$tmp_D = intval(substr($time, 8,2));
				if($tmp_D < $Day) continue;

				$tmp_H = intval(substr($time, 11,2));
				if($tmp_H < $Hour) continue;

				$Data[] = ($element->GetLoc())[0];
			}
			return $Data;
		}

		function Get24Hr_Lng(){
			$Year = intval(date("Y"));
			$Month = intval(date("m"));
			$Day = intval(date("d"));
			$Hour = intval(date("H"));

			$Diff = $Hour - 24;
			$Day = $Day -1;
			$Hour = $Hour + $Diff;

			foreach ($this->DataBase as $element) {
				$time = $element->GetTime();

				$tmp_Y = intval(substr($time, 0,4));
				if($tmp_Y < $Year) continue;

				$tmp_M = intval(substr($time, 5,2));
				if($tmp_M < $Month) continue;

				$tmp_D = intval(substr($time, 8,2));
				if($tmp_D < $Day) continue;

				$tmp_H = intval(substr($time, 11,2));
				if($tmp_H < $Hour) continue;

				$Data[] = ($element->GetLoc())[1];
			}
			return $Data;
		}

		function GetAll_Lat(){
			
			foreach ($this->DataBase as $element) {
				$Data[] = ($element->GetLoc())[0];
			}
			return $Data;
		}

		function GetAll_Lng(){
			
			foreach ($this->DataBase as $element) {
				$Data[] = ($element->GetLoc())[1];
			}
			return $Data;
		}

		function DivideRowData($rowdata){

			$count = 0;
			$array = explode("},",$rowdata);
			foreach($array as $element){
				$parts = explode(",", $element);
				$lat = substr($parts[3], 7,-1);
				$lng = substr($parts[4], 7,-1);

				if(!strcmp($lat, "ul")) continue;

				$lat = floatval($lat);
				$lng = floatval($lng);

				$time = substr($parts[5], 14,-1);

				$this->DataBase[] = new Data(array($lat,$lng),$time);

				//echo $time ."<br>";
			}
		}

		function GetNewest_Loc(){
			$Newest = end($this->DataBase);
			return $Newest->GetLoc();
		}

		function GetNewest_Time(){
			$Newest = end($this->DataBase);
			return $Newest->GetTime();
		}
	}
?>