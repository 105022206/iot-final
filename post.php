<?php
session_start();
require("lib_DB.php");

?>

<?php
$DB = new DB();
$DB->RequestData();
?>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <style type="text/css">
    #map-canvas {
      width: 80%;
      height: 80%;
      margin: 0;
      padding: 0;
    }
  </style>

  <title>肥宅狗狗去哪ㄦ混</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCFNRUyTL0CaYZwcBFkfS4jnlYYeM8p0O4">
  </script>
  <script type="text/javascript">
    function initialize() {
      console.log("init")
      document.getElementById('time').innerHTML='最新資料時間<?php echo($DB->GetNewest_Time())?>';
;

      var mapOptions = {
        center: {
          lat: 24.793429,
          lng: 120.993823
        },
        zoom: 17,
        mapTypeControl: true,
        fullscreenControl: true,
        rotateControl: true,
        scaleControl: true,
        streetViewControl: true,
        zoomControl: true,
        mapTypeControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER
        },
        fullscreenControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        rotateControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
        },
        scaleControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        streetViewControlOptions: {
          position: google.maps.ControlPosition.TOP_LEFT
        },
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        }
      };

      var map = new google.maps.Map(
        document.getElementById('map-canvas'),
        mapOptions);
      var marker = new google.maps.Marker({
        map: map,
        position: {
          lat: <?php echo ($DB->GetNewest_Loc())[0] ?>,
          lng: <?php echo ($DB->GetNewest_Loc())[1] ?>
        },
      });
      // var marker2 = new google.maps.Marker({
      //     map: map,
      //     position:  {
      //         lat: 24.794429,
      //         lng: 120.993223
      //     },
      // });


    }

    function Past_data() {
      console.log("past");

      var lat_data = [];
      var lon_data = [];
      var data_lat = <?php echo json_encode(($DB->GetAll_Lat())) ?>;
      var data_lon = <?php echo json_encode(($DB->GetAll_Lng())) ?>;


      data_lat.forEach(function(element) {
        lat_data.push(element);
      })
      data_lon.forEach(function(element) {
        lon_data.push(element);
      })
      var len = lat_data.length;
      var mapOptions = {
        center: {
          lat: 24.793429,
          lng: 120.993823
        },
        zoom: 17,
        mapTypeControl: true,
        fullscreenControl: true,
        rotateControl: true,
        scaleControl: true,
        streetViewControl: true,
        zoomControl: true,
        mapTypeControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER
        },
        fullscreenControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        rotateControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
        },
        scaleControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        streetViewControlOptions: {
          position: google.maps.ControlPosition.TOP_LEFT
        },
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        }
      };

      var map = new google.maps.Map(
        document.getElementById('map-canvas'),
        mapOptions);
      for (i = 0; i < len; i++) {
        var marker = new google.maps.Marker({
          map: map,
          position: {
            lat: lat_data[i],
            lng: lon_data[i]
          },
          icon: {
            path: google.maps.SymbolPath.CIRCLE,
            strokeColor: "blue",
            strokeWeight: 3,
            fillColor: "red",
            fillOpacity: 0.3,
            scale: 2,
          },
        });
      }




    }

    function DAY_data() {
      console.log("Day");
      var lat_data = [];
      var lon_data = [];
      var data_lat = <?php echo json_encode(($DB->Get24Hr_Lat())) ?>;
      var data_lon = <?php echo json_encode(($DB->Get24Hr_Lng())) ?>;


      //console.log(data_lat);
      data_lat.forEach(function(element) {
        console.log(element);
        lat_data.push(element);
      })
      data_lon.forEach(function(element) {
        console.log(element);
        lon_data.push(element);
      })
      var len = lat_data.length;






      var mapOptions = {
        center: {
          lat: 24.793429,
          lng: 120.993823
        },
        zoom: 17,
        mapTypeControl: true,
        fullscreenControl: true,
        rotateControl: true,
        scaleControl: true,
        streetViewControl: true,
        zoomControl: true,
        mapTypeControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER
        },
        fullscreenControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        rotateControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
        },
        scaleControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        streetViewControlOptions: {
          position: google.maps.ControlPosition.TOP_LEFT
        },
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        }
      };

      var map = new google.maps.Map(
        document.getElementById('map-canvas'),
        mapOptions);


      for (i = 0; i < len; i++) {
        var marker = new google.maps.Marker({
          map: map,
          position: {
            lat: lat_data[i],
            lng: lon_data[i]
          },
        });
      }




    }

    function ChangeBackground() {
      var idx = document.getElementById("bg_dog");
      var new_picture = document.getElementById("dog_select").value;
      console.log(new_picture)
      idx.style.background = "url('./img/home-bg.jpg') no-repeat center";
      idx.style.backgroundSize = "100%"
      if (new_picture == "CiaoCiao") {
        idx.style.background = "url('./img/CiaoCiao.jpg') no-repeat center";
        idx.style.backgroundSize = "100%"
        //idx.style.backgruoundPosition="center"     
      } else if (new_picture == "CiaoHu") {
        idx.style.background = "url('./img/CiaoHu.jpeg') no-repeat center";
        idx.style.backgroundSize = "100%"
      } else if (new_picture == "GuaiGuai") {
        idx.style.background = "url('./img/GuaiGuai.jpg') no-repeat center";
        idx.style.backgroundSize = "100%"
      } else if (new_picture == "HuaJuan") {
        idx.style.background = "url('./img/HuaJuan.jpg') no-repeat center";
        idx.style.backgroundSize = "100%"
      } else if (new_picture == "Jimmy") {
        idx.style.background = "url('./img/Jimmy.jpg') no-repeat center";
        idx.style.backgroundSize = "100%"
      } else if (new_picture == "MaiYa") {
        idx.style.background = "url('./img/MaiYa.jpg') no-repeat center";
        idx.style.backgroundSize = "100%"
      } else if (new_picture == "MaoBi") {
        idx.style.background = "url('./img/MaoBi.jpg') no-repeat center";
        idx.style.backgroundSize = "100%"
      }


    }

    function init() {


      var idx = document.getElementById('select').value;
      if (idx == "new") {
        initialize()
      } else if (idx == "past") {
        Past_data();
      } else if (idx == "day") {
        DAY_data();
      }
    }


    window.onload = init;
  </script>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.html">肥宅狗狗去哪ㄦ混</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.html">首頁</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.html">關於</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="post.html">狗狗在哪ㄦ</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://my.nthu.edu.tw/~srv9215/carelife2016">懷生社</a>
          </li>
        </ul>
      </div>
      <div>
        <select class="form-control" style="margin-top: 30px" id="dog_select" onchange="ChangeBackground()">
          <option value='menu'>狗狗名冊</option>
          <option value='CiaoCiao'>巧巧</option>
          <option value='CiaoHu'>巧虎</option>
          <option value='GuaiGuai'>乖乖</option>
          <option value='HuaJuan'>花捲</option>
          <option value='Jimmy'>布丁</option>
          <option value='MaiYa'>麥芽</option>
          <option value='MaoBi'>毛筆</option>





        </select>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead">
    <div class="overlay" style="background-image: url('./img/home-bg.jpg'); background-repeat:no-repaet" id='bg_dog'></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
            <br><br><br><br><br><br><br><br><br>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Post Content -->

  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <br>
        <br>


      </div>
    </div>
    <div id="time" style="position: relative; left:100px"></div>
    <div id="map-canvas" style="position: relative; left:100px"></div>
    <select class="form-control" style="margin-top: 30px; width: 100px; position: relative; left: 700px" id="select" onchange="init()">
      <option value="new">最新位置</option>
      <option value="past">歷史資料</option>
      <option value="day">一天活動</option>

    </select>
  </div>


  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>