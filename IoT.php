<?php
	session_start();
	require("lib_DB.php");
	
?>

<?php
	$DB = new DB();
	$Data = $DB->RequestData();
	echo $DB->GetNewest_Time();
?>

<html>

<head>
    <meta charset="utf-8" />
    <style type="text/css">
        html,
        body,
        #map-canvas {
            width:80%;
            height: 80%;
            margin: 0;
            padding: 0;
        }
    </style>
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCFNRUyTL0CaYZwcBFkfS4jnlYYeM8p0O4">
    </script>2
    <script type="text/javascript">
        function initialize() {
			console.log("init")
            var mapOptions = {
                center: {
                    lat: 24.793429,
                    lng: 120.993823
                },
                zoom: 17,
                mapTypeControl: true,
                fullscreenControl: true,
                rotateControl: true,
                scaleControl: true,
                streetViewControl: true,
                zoomControl: true,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                fullscreenControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                rotateControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_CENTER
                },
                scaleControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                },
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                }
            };

            var map = new google.maps.Map(
                document.getElementById('map-canvas'),
                mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                position:  {
                    lat: <?php echo ($DB->GetNewest_Loc())[0] ?>,
                    lng: <?php echo ($DB->GetNewest_Loc())[1] ?>
                },
            });
            // var marker2 = new google.maps.Marker({
            //     map: map,
            //     position:  {
            //         lat: 24.794429,
            //         lng: 120.993223
            //     },
            // });


        }

        function Past_data() {
			console.log("past");
            var mapOptions = {
                center: {
                    lat: 24.793429,
                    lng: 120.993823
                },
                zoom: 17,
                mapTypeControl: true,
                fullscreenControl: true,
                rotateControl: true,
                scaleControl: true,
                streetViewControl: true,
                zoomControl: true,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                fullscreenControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                rotateControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_CENTER
                },
                scaleControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                },
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                }
            };

            var map = new google.maps.Map(
                document.getElementById('map-canvas'),
                mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                position:  {
                    lat: 24.793429,
                    lng: 120.993423
                },
            });
            var marker2 = new google.maps.Marker({
                map: map,
                position:  {
                    lat: 24.794429,
                    lng: 120.993223
                },
			});
			var marker3 = new google.maps.Marker({
                map: map,
                position:  {
                    lat: 24.794629,
                    lng: 120.992223
                },
                icon:{
                    path:google.maps.SymbolPath.CIRCLE,
                    strokeColor:"blue",
                    strokeWeight:3,
                    fillColor:"red",
                    fillOpacity:0.3,
                    scale:2,
                },
			});
            var marker4 = new google.maps.Marker({
                map: map,
                position:  {
                    lat: 24.794229,
                    lng: 120.992223
                },
                icon:{
                    path:google.maps.SymbolPath.CIRCLE,
                    strokeColor:"blue",
                    strokeWeight:3,
                    fillColor:"red",
                    fillOpacity:0.3,
                    scale:2,
                },
			});
			  


		}
		function init(){

		
		var idx=document.getElementById('select').value;
		console.log(idx);
		if(idx=="new")
        {
			initialize()
		}
		else if(idx=="past"){
			console.log("test")
		Past_data();
			}
		}

        
		window.onload=init;
    </script>
</head>

<body>
    <div id="map-canvas"></div>
    <div id="bar"><form>
        <select name="YourLocation"  id="select" onchange="init()">
        　<option value="past">台北</option>
        　<option value="new">桃園</option>
        　<option value="Hsinchu">新竹</option>
        　<option value="Miaoli">苗栗</option>
        　...
        </select>
        </form>
        </div>
</body>

</html>
